# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* BIGbirddy's box
* Key functions (add/delete)
    1. 簡易論壇
    2. 簡易聊天室

* Other functions (add/delete)
    1. 具有分類/標題的發文系統
    2. 可以更改個人頁面大頭貼
    3. 聊天室提供回復者時區以供辨別

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
使用firebase中的auth來製作會員系統，除了可以
用email登入外，提供google的第三方登入系統，
使用realtime databae製作論壇以及聊天室，在論壇中
可以選擇發表新的貼文，或者是進入其他人的文章中查
看並且回復，另外會員可在下拉選單中選取個人頁面更
改會員資訊。


## Security Report (Optional)
在會員登入前，無法查看任何資訊，登入後
僅能透過文章或聊天系統更改資料庫裡的內容，無法任意
修改或刪除。