var user_email;
var user_image;
var user_name;

function init() {
    user_email = '';
    user_image = '';

    document.getElementById("comment-container").style.display = "none";
    document.getElementById("userpage_div").style.display = "none";
    document.getElementById("return_btn").onclick = function () {
        // redirect
        window.location = "index.html";
    };
    document.getElementById("confirm_btn").onclick = function () {
        // update user info
        var currUser = firebase.auth().currentUser;
        var nameStr = document.getElementById("name_content").value;
        var photoURLStr = document.getElementById("photoURL_content").value;
        var profile =
            {
                displayName: nameStr,
                photoURL: photoURLStr
            };
        if (nameStr && photoURLStr) {
            currUser.updateProfile(profile);
        }
        nameStr = "";
        photoURLStr = "";

        // redirect
        window.location = "index.html";
    };


    /* Handle category buttons */

    // Get elements
    const forumButton = document.getElementById("forum_button");
    const chatRoomButton = document.getElementById("chatroom_button");
    var cateBgObj = document.getElementById("cate_bg");
    var cateNameObj = document.getElementById("category_name");
    var cateDescriptObj = document.getElementById("category_description");
    var forumDiv = document.getElementById("forum_div");
    var chatroomDiv = document.getElementById("chatroom_div");
    chatroomDiv.style.display = "none";

    // Change category when pressing buttons
    chatRoomButton.addEventListener("click", function () {
        cateBgObj.classList.remove("bg-change-b-to-p");
        cateBgObj.classList.add("bg-change-p-to-b");
        cateNameObj.innerText = "Chat Room";
        cateDescriptObj.innerText = "Exchange with some friends."
        chatroomDiv.style.display = "block";
        forumDiv.style.display = "none";
    });

    forumButton.addEventListener("click", function () {
        cateBgObj.classList.remove("bg-change-p-to-b");
        cateBgObj.classList.add("bg-change-b-to-p");
        cateNameObj.innerText = "Forum";
        cateDescriptObj.innerText = "Post your thought."
        forumDiv.style.display = "block";
        chatroomDiv.style.display = "none";
    });



    /* Handle login & logout */
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            if (user.displayName) {
                user_name = user.displayName;
            } else {
                user_name = "[edit below]";
            }
            if (user.photoURL) {
                user_image = user.photoURL;

            } else {
                user_image = "img/test.svg";
            }  
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span >"
                + "<span class='dropdown-item' id='userpage-btn'>User page</span>"
                + "<span class='dropdown-item' id='logout-btn'>Logout</span>";

            var userpage_btn = document.getElementById("userpage-btn");
            userpage_btn.addEventListener("click", function () {
                document.getElementById("forum_div").style.display = "none";
                document.getElementById("chatroom_div").style.display = "none";
                document.getElementById("userpage_div").style.display = "block";

                var pageContent = document.getElementById("userpage_content");
                pageContent.innerHTML = "<img src='" + user_image + "' alt='' class='mr-2 rounded'"
                    + " style='height:32px; width:32px;'>"
                    + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
                    + "<b class='d-block text-gray-dark'>Email: </b>" + user_email
                    + "<b class='d-block text-gray-dark'>Name: </b>" + user_name + "</p>";
            });

            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });



    /* Handle posts */

    // Get elements
    var post_btn = document.getElementById('post_btn');
    var post_cat = document.getElementById('new-post-category');
    var post_title = document.getElementById('new-post-title');
    var post_txt = document.getElementById('new-post-content');

    // Store to database when add post
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set(
                {
                    email: user_email,
                    avatar: user_image,
                    category: post_cat.value,
                    title: post_title.value,
                    data: post_txt.value
                }
            );
            post_cat.value = "";
            post_title.value = "";
            post_txt.value = "";
        }
    });

    // Show content when database update
    var str_before_username_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'><a href='#' id='";
    var str_before_username_11 = "'>[";
    var str_before_username_2 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='"
    var str_before_username_22 = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    postsRef.once('value')      // show comments that exist for once
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username_1
                    + childSnapshot.key + str_before_username_11
                    + childData.category + "] " + childData.title + "</a>"
                    + str_before_username_2 + childData.avatar + str_before_username_22
                    + childData.email + "</strong>" + childData.data
                    + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            setLink("all");

            postsRef.on('child_added', function (data) {    // show added comment anytime
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    console.log(childData);
                    total_post[total_post.length] = str_before_username_1
                        + childData.key + str_before_username_11
                        + childData.category + "] " + childData.title + "</a>"
                        + str_before_username_2 + childData.avatar + str_before_username_22
                        + childData.email + "</strong>" + childData.data
                        + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    setLink(childData.key);
                }
            });
        })
        .catch(e => console.log(e.message));



    /* Handle chat */

    // Get elements
    var send_btn = document.getElementById('send_btn');
    var send_txt = document.getElementById('chat');

    // Store to database when post comments
    send_btn.addEventListener('click', function () {

        if (send_txt.value != "") {
            var newpostref = firebase.database().ref('chat_list').push();
            var currentTime = Date.now();
            newpostref.set({
                email: user_email,
                data: send_txt.value,
                timestamp: currentTime
            });
            send_txt.value = "";
        }
    });

    // Show content when database update
    var str_before_username = 
        "<div class='media text-muted pt-3'>"
        + "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>"
        + "<bold class='d-block text-gray-dark'>";
    var str_after_content_2 = "</p></div>\n";

    var chatsRef = firebase.database().ref('chat_list');
    var total_chat = [];
    var first_count_2 = 0;
    var second_count_2 = 0;

    chatsRef.once('value')      // show chats that exist for once
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_chat[total_chat.length] = str_before_username + "<b>"
                    + childData.email + "</b> at " + new Date(childData.timestamp)
                    + "</br><span style='font-size:medium'>" + childData.data + "</span>"
                    + str_after_content_2;
                first_count_2 += 1;
            });
            document.getElementById('chatroom').innerHTML = total_chat.join('');

            // scroll down to the bottom
            var div = document.getElementById("chatroom_container");
            div.scrollTop = div.scrollHeight - div.clientHeight;

            chatsRef.on('child_added', function (data) {    // show added chat anytime
                second_count_2 += 1;
                if (second_count_2 > first_count_2) {
                    var childData = data.val();
                    total_chat[total_chat.length] = str_before_username + "<b>"
                        + childData.email + "</b> at " + new Date(childData.timestamp)
                        + "</br><span class='mb-0 lh-100'>" + childData.data + "</span>"
                        + str_after_content_2;
                    document.getElementById('chatroom').innerHTML = total_chat.join('');

                    // scroll down to the bottom
                    var div = document.getElementById("chatroom_container");
                    div.scrollTop = div.scrollHeight - div.clientHeight;
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}

function setLink(id)
{
    if (id == "all") {
        // Iterate comment list
        var query = firebase.database().ref("com_list").orderByKey();
        query.once("value")
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    setLink(childSnapshot.key);
                });
            });

    } else {
        // Add listenr for specific id
        var domObj = document.getElementById(id);
        domObj.addEventListener("click", function () {
            showPost(id);
        });
    }
}

function showPost(id)
{
    // Change post div
    document.getElementById("post-container").style.display = "none";
    document.getElementById("comment-container").style.display = "block";

    // Update database when comment
    var btn = document.getElementById("comment_btn");
    var txt = document.getElementById("comment-content");
    btn.addEventListener("click", function () {
        if (txt.value != "") {
            var commentRef = firebase.database().ref('com_list/' + id).child("comments").push();
            var userData = firebase.auth().currentUser;

            commentRef.set(
                {
                    email: user_email,
                    avatar: user_image,
                    data: txt.value
                }
            );
            txt.value = "";
        }
    });

    // Show only that post and its comments
    var postsRef = firebase.database().ref("com_list/" + id);
    var total_post = [];

    // show post
    var str_before_username_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'>["
    var str_before_username_2 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='";
    var str_before_username_8 = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    postsRef.once("value", function (snapshot) {
        var data = snapshot.val();
        total_post[total_post.length] = str_before_username_1
            + data.category + "] " + data.title
            + str_before_username_2 + data.avatar + str_before_username_8
            + data.email + "</strong>" + data.data
            + str_after_content;
    });


    // show comments
    var commentRef = postsRef.child("comments");

    var first_count = 0;
    var second_count = 0;
    var post_number = 0;

    var str_before_username_3 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'>#"
    var str_before_username_4 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='";
    var str_before_username_5 = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content_2 = "</p></div></div>\n";

    commentRef.once('value')      // show comments that exist for once
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                post_number += 1;
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username_3
                    + post_number + str_before_username_4 + childData.avatar + str_before_username_5
                    + childData.email + "</strong>" + childData.data
                    + str_after_content_2;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            commentRef.on('child_added', function (data) {    // show added comment anytime
                second_count += 1;
                if (second_count > first_count) {
                    post_number += 1;
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username_3
                        + post_number + str_before_username_4 + childData.avatar + str_before_username_5
                        + childData.email + "</strong>" + childData.data
                        + str_after_content_2;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}